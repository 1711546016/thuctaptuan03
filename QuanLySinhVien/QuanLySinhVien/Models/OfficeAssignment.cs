﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuanLySinhVien.Models
{
    public class OfficeAssignment
    {
        public int Id { get; set; }
        public string Location { get; set; }

        public Instructor Instructor { get; set; }
    }
}
