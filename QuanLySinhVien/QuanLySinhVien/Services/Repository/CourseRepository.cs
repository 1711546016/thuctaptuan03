﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using QuanLySinhVien.DAL;
using QuanLySinhVien.Models;
using QuanLySinhVien.Services.IRepository;
using Microsoft.EntityFrameworkCore;

namespace QuanLySinhVien.Services.Repository
{
    public class CourseRepository : Repository<Course>, ICourseRepository
    {
        public CourseRepository(LeLeContext leLeContext) : base(leLeContext)
        {
        }

        public IEnumerable<Course> CoursesToDepartment()
        {
            return LeLeContext.Courses.Include(x => x.Department).ToList();
        }
    }
}
