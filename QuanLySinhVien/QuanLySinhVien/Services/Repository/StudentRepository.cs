﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using QuanLySinhVien.DAL;
using QuanLySinhVien.Models;
using QuanLySinhVien.Services.IRepository;
using Microsoft.EntityFrameworkCore;

namespace QuanLySinhVien.Services.Repository
{
    public class StudentRepository : Repository<Student>, IStudentRepository
    {
        public StudentRepository(LeLeContext leLeContext) : base(leLeContext)
        {
        }


        public IEnumerable<Enrollment> CoursesToStudent(int studentId)
        {
            return LeLeContext.Enrollments
                .Where(e => e.StudentId == studentId)
                .Include(x => x.Student)
                .Include(x => x.Course)
                .ToList();
        }
        public IQueryable<Student> GetStudents()
        {
            return LeLeContext.Students.AsQueryable();
        }
    }
}
