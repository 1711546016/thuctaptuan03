﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using QuanLySinhVien.DAL;
using QuanLySinhVien.Models;
using QuanLySinhVien.Services.IRepository;

namespace QuanLySinhVien.Services.Repository
{
    public class EnrollmentRepository : Repository<Enrollment>, IEnrollmentRepository
    {
        public EnrollmentRepository(LeLeContext leLeContext) : base(leLeContext)
        {
        }
    }
}
