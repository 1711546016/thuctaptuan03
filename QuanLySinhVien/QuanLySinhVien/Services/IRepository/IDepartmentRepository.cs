﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using QuanLySinhVien.Models;

namespace QuanLySinhVien.Services.IRepository
{
    public interface IDepartmentRepository : IRepository<Department>
    {
        IEnumerable<Department> InstructorToDepartments();

        Department InstructorToDepartment(int id);
    }
}
