﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using QuanLySinhVien.Models;

namespace QuanLySinhVien.Services.IRepository
{
    public interface IInstructorRepository : IRepository<Instructor>
    {
        Task<IEnumerable<Instructor>> Instructors();
        Task<Instructor> Instructor(int id);
    }
}
